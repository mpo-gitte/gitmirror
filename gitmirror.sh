#!/bin/bash
#
# gitmirror.
#
# Mirror repositories.
#
# @author pilgrim.lutz@imail.de
# @version 2023-05-27

. ./.settings

workdir=/tmp/$1
currentdir=`pwd`

clear

rm -f -r $workdir/

git clone --mirror ${giturl}/$1 $workdir

cd $workdir

git push --mirror ${codebergurl}/$1

cd $currentdir

rm -f -r $workdir/