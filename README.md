# How I mirror my repositories to Codeberg

This little shell script mirrors my local repositories to Codeberg.

I call it with the name of the local repository. It pushes then the 
changes to an existing repository with the same name on Codeberg.

Some settings are made in .settings . SSH Keys have to be set correctly.
